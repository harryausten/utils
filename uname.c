#include <stdbool.h>
#include <sys/utsname.h>
#include <unistd.h>

#include "utils.h"

int main(int argc, char *argv[])
{
	bool m, n, r, s, v;
	struct utsname uts;
	int ret, opt;

	m = n = r = s = v = false;

	while ((opt = getopt(argc, argv, "amnoprsv")) != -1) {
		switch (opt) {
		case 'a':
			m = n = r = s = v = true;
			break;
		case 'm':
			m = true;
			break;
		case 'n':
			n = true;
			break;
		case 'r':
			r = true;
			break;
		case 's':
			s = true;
			break;
		case 'v':
			v = true;
			break;
		default:
			fprintf(stderr, "Usage: %s [-r]\n", argv[0]);
			return EXIT_FAILURE;
		}
	}

	ret = uname(&uts);
	if (ret)
		err("uname");

	if (!(m || n || r || s || v)) {
		puts(uts.sysname);
		return EXIT_SUCCESS;
	}

	if (s)
		printf("%s ", uts.sysname);
	if (n)
		printf("%s ", uts.nodename);
	if (r)
		printf("%s ", uts.release);
	if (v)
		printf("%s ", uts.version);
	if (m)
		printf("%s ", uts.machine);

	putchar('\n');

	return EXIT_SUCCESS;
}
