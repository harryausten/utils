CFLAGS = -Wall -Wextra -Wpedantic -Werror -O3 -march=native
PROGS = $(basename $(wildcard *.c))

.PHONY: all
all: $(PROGS)

.PHONY: clean
clean:
	$(RM) $(PROGS)
