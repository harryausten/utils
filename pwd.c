#include <limits.h>
#include <sys/stat.h>
#include <unistd.h>

#include "utils.h"

static struct stat cst, pst;

static const char *pwd(const char *cwd)
{
	const char *env = getenv("PWD");

	return (env && env[0] == '/' && !stat(env, &pst)
		&& pst.st_dev == cst.st_dev && pst.st_ino == cst.st_ino)
		? env : cwd;
}

int main(int argc, char *argv[])
{
	char cwd[PATH_MAX];
	const char *print = cwd;
	char mode = 'L';
	int opt;

	while ((opt = getopt(argc, argv, "LP")) != -1) {
		switch(opt) {
		case 'L':
		case 'P':
			mode = opt;
			break;
		default:
			fprintf(stderr, "Usage: %s [-LP]\n", argv[0]);
			return EXIT_FAILURE;
		}
	}

	if (!getcwd(cwd, PATH_MAX))
		err("getcwd");

	if (stat(cwd, &cst))
		err("stat");

	if (mode == 'L')
		print = pwd(cwd);

	puts(print);
	return EXIT_SUCCESS;
}
