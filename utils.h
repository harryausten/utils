#ifndef __UTILS_H__
#define __UTILS_H__

#include <stdio.h>
#include <stdlib.h>

static inline void err(const char *s)
{
	perror(s);
	exit(EXIT_FAILURE);
}

#endif
