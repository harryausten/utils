#include <dirent.h>
#include <string.h>
#include <unistd.h>

#include "utils.h"

static inline int no_dot_dirs(const struct dirent *d)
{
	const char *s = d->d_name;

	return (strcmp(s, ".") != 0 && strcmp(s, "..") != 0);
}

static inline int no_hidden_files(const struct dirent *d)
{
	return strncmp(d->d_name, ".", 1);
}

static int (*filt)(const struct dirent *d) = no_hidden_files;

static void print_dir(const char *path)
{
	struct dirent **ents;
	int n;

	n = scandir(path, &ents, filt, alphasort);
	if (n == -1)
		err("scandir");

	for (int i = 0; i < n; ++i) {
		puts(ents[i]->d_name);
		free(ents[i]);
	}
}

int main(int argc, char *argv[])
{
	char cwd[PATH_MAX];
	int opt;

	while ((opt = getopt(argc, argv, "aA")) != -1) {
		switch (opt) {
		case 'a':
			filt = NULL;
			break;
		case 'A':
			filt = no_dot_dirs;
			break;
		default:
			fprintf(stderr, "Usage: %s [filepath]...\n", argv[0]);
			return EXIT_FAILURE;
		}
	}

	switch (argc - optind) {
	case 0:
		if (!getcwd(cwd, PATH_MAX))
			err("getcwd");
		print_dir(cwd);
		break;
	case 1:
		print_dir(argv[optind]);
		break;
	default:
		for (int i = argc - 1; i >= optind; --i) {
			printf("%s:\n", argv[i]);
			print_dir(argv[i]);
			if (i > optind)
				putchar('\n');
		}
		break;
	}

	return EXIT_SUCCESS;
}
